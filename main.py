from config import PATH_FILES, PATH_PLOTS
import os
from utils import extract_text_from_pdf, clean_text_vect, plot_word_cloud


def generate_word_clouds():
    files = os.listdir(PATH_FILES)
    for f in files:
        if f.endswith("program.pdf"):
            name = f.split("_")[1]
            time_id = f.split("_")[0]
            year = time_id[0:4]
            v = time_id[-1]
            title = "{} - {} ({}° vuelta)".format(name.upper(), year,v)
            print(title)
            color, bg_color = None, None
            if name == "kast":
                color = "rainbow"
                bg_color = "navy"
            elif name == "boric":
                color = "Pastel1"
                bg_color = "salmon"
            path = str(PATH_FILES / f)
            path_save = PATH_PLOTS / "{}_{}.png".format(time_id, name)
            if path_save.exists():
                print("{} exists, skipping".format(path_save))
                continue
            text = extract_text_from_pdf(path)
            text_clean = clean_text_vect(text)
            plot_word_cloud(path_save=path_save, text=text_clean, path_mask=PATH_FILES / "comment.png",
                            color=color, bg_color=bg_color, title=title)


if __name__ == '__main__':
    generate_word_clouds()
