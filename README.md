# Chile Presidential 2021


## Motivation

In November 2021 were the presidential elections in Chile and in this context I made the exercise of visualizing the presidential programs of Jose Antonio Kast and Gabriel Boric (Boric 2nd round I did not find) using the classic word cloud. 

Without wanting to make any kind of campaign, I just wanted to share the reflection on the presidential programs, which are extremely long (150 pages on average) and not exactly a light reading, I doubt that many voters read them, being this a key input to vote more informed. Under this context: Is there another way to visualize the programs that will allow you to have a better idea of who to vote for? Are the electoral slots, debates and propaganda enough to inform about the candidates' programs? Can analytics tools play a role in that sense?

