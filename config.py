import os
from pathlib import Path

# Paths of projects
PATH_MAIN = Path(os.path.dirname(__file__))
PATH_FILES = PATH_MAIN / "files"
PATH_PLOTS = PATH_MAIN / "plots"

