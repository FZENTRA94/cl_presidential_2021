from tika import parser
import io
from pdfminer.converter import TextConverter
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfpage import PDFPage
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from config import PATH_FILES, PATH_PLOTS
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import matplotlib.pyplot as plt
from wordcloud import ImageColorGenerator
import numpy as np
from PIL import Image
from subprocess import call



ALPHABET = "abcdefghijklmnopqrstuvwxyz"
SPA_CHARS = "áéíóúüñ"

def read_pdf(path, show_first=100):
    print("Reading file", path)
    raw = parser.from_file(path)
    print(raw['content'][0:show_first])
    return

def extract_text_from_pdf(pdf_path):
    # reference: http://www.blog.pythonlibrary.org/2018/05/03/exporting-data-from-pdfs-with-python/
    #call('qpdf --password=%s --decrypt %s %s' % ('', pdf_path, pdf_path), shell=True)
    resource_manager = PDFResourceManager()
    fake_file_handle = io.StringIO()
    converter = TextConverter(resource_manager, fake_file_handle)
    page_interpreter = PDFPageInterpreter(resource_manager, converter)
    with open(pdf_path, 'rb') as fh:
        for page in PDFPage.get_pages(fh,
                                      caching=True,
                                      check_extractable=True):
            page_interpreter.process_page(page)
        text = fake_file_handle.getvalue()
    # close open handles
    converter.close()
    fake_file_handle.close()
    if text:
        print("pdf {} read, sample:".format(pdf_path))
        print(text[0:100])
        return text

def clean_text_vect(text):
    text_vect = text.split()
    text_vect_clean = []
    stop_words = set(stopwords.words('spanish'))

    for w in text_vect:
        w_ = w.lower()
        w_ = "".join([c for c in w_ if c in ALPHABET + SPA_CHARS])
        if w_ not in stop_words:
            text_vect_clean.append(w_)

    print("Total words:", len(text_vect_clean))
    return " ".join(text_vect_clean)

def plot_word_cloud(path_save, text, path_mask, color="rainbow", bg_color="navy", title="Word Cloud"):
    mask = np.array(Image.open(path_mask))
    #plt.imshow(mask)
    wordcloud = WordCloud(mask=mask,width=3000, height=2000, max_words=10000,background_color=bg_color,
                          colormap=color, collocations=False).generate(text)
    #image_colors = ImageColorGenerator(mask)
    plt.figure(figsize=(30, 25), facecolor=bg_color)
    plt.imshow(wordcloud)
    plt.axis("off")
    plt.title(title, fontsize=45,  y=0.95, pad=-10, color="white")
    plt.tight_layout()

    #plt.imshow(wordcloud.recolor(color_func=image_colors), interpolation="bilinear")
    plt.savefig(path_save, face_color=bg_color, transparent=True)
    print("plot saved in", path_save)



if __name__ == '__main__':
    text_sample = extract_text_from_pdf(PATH_FILES / "lorem_Ipsum.pdf")
    text_vect_sample = clean_text_vect(text_sample)
    plot_word_cloud(PATH_PLOTS / "lorem_Ipsum.png", text=text_vect_sample, path_mask=PATH_FILES/"comment.png")
    print(text_vect_sample)